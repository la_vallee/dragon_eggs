require "app/counter/counter_a.rb"

class Game

  QUESTIONS = [
    "Est-ce que le ciel est bleu ?",
    "Est-ce que l'eau est mouillée ?",
    "Est-ce que les chats ont des griffes ?",
    "Est-ce que les voitures ont des roues ?",
    "Est-ce que les humains peuvent voler ?",
  ]

  def initialize args
    @args      = args
    @counter_a = CounterA.new args
    init_game
  end

  def args
    @args
  end

  def state
    args.state
  end

  #
  # TODO create 3 different type of count_down
  #
  # 1 avec la forme la plus naive
  # 2 en explorant les methode de doc que je ne comprend pas.
  #
  # 1 tick et tick_count
  # 2 just afficher
  # 3 avec le module.
  #
  # Time.now
  #
  def tick
    @counter_a.tick

    args.outputs.labels  << [640, 540, 'Salut les gens', 5, 1]
    args.outputs.sprites << { x:      576,
                              y:      280,
                              w:      128,
                              h:      101,
                              path:   'dragonruby.png',
                              angle:  state.tick_count }

    # Si l'utilisateur a répondu à la question, passer à la question suivante
    if state.answered
      state.current_question += 1
      state.answered         =  false
    end

    check_keyboard

    # Affichage de la question
    current_question    =  QUESTIONS[state.current_question]
    args.outputs.labels << [200, 250, current_question, 5, 1]

    # Affichage de la réponse si l'utilisateur a répondu à la question précédente
    if state.current_question > 0 && state.answered
      answer = state.last_answer ? "Oui" : "Non"
      args.outputs.labels << [200, 200, "Réponse : #{answer}", 5, 1]
    end

    # initialise la variable d'angle et de temps
    @angle ||= 0
    @last_tick ||= Time.now

    # dessine un carré rouge de 100 x 100 pixels, centré à l'écran, avec un angle de rotation de @angle degrés
    args.outputs.solids << [args.grid.center_x - 50, args.grid.center_y - 50, 100, 100, 255, 0, 0, @angle]

    # ajuste l'angle de rotation toutes les trois secondes
    if Time.now - @last_tick > 3
      @angle += 90
      @angle %= 360
      @last_tick = Time.now
    end

  end

  private

  def check_keyboard
    # Vérification de la réponse de l'utilisateur
    if args.inputs.keyboard.key_down.n
      state.last_answer = false
      state.answered    = true
    elsif args.inputs.keyboard.key_down.y
      state.last_answer = true
      state.answered    = true
    end
    if args.inputs.keyboard.key_down.escape
      args.gtk.request_quit
    end
    if args.inputs.keyboard.key_down.r
      @counter_a.reset
    end
  end

  def init_game
    if state.tick_count == 0
      state.current_question = 0
      state.answered         = true
    end
  end
end
