#
# counter can be used in counter for continue on vidéo game (street fighter, metal slug)
# for level time duration (mario)
#
# But also internaly to bettween to step of:
# - animation
# - tween
# - scene change
# - menu
# - text scrolling
#
class CounterA

  def initialize args
    @args = args
    init
  end

  def args
    @args
  end

  def state
    args.state
  end

  def tick
    args.outputs.labels << [100, 100, "CounterA : #{time_left}", 30]
    #args.outputs.labels << [100, 100, "CounterA : #{time_left}", 30, {r: 255, g: 120, b: 200, a: 255, style: :bold}]
    update
  end

  def init
    @seconds = 6
    reset
  end

  def count_over?
    state.count_down < 0
  end

  #
  # NOTE: idiv make count_down < 0 hard predictable
  #
  def time_left
    state.count_down.idiv(60) # + 1
  end

  def update
    unless count_over?
      state.count_down -= 1
    end
  end

  def reset
    state.count_down = @seconds * 60
  end
end
